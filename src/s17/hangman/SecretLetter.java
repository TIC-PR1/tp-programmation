package s17.hangman;

public class SecretLetter extends Letter {

    public SecretLetter(char c) {
        super(c);
    }

    public char shownLetter() {
        return '_';
    }

    @Override
    public final boolean isSecret() {
        return true;
    }

}
