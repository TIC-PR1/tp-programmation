package s19.circles;

// Variant B, using composition
public class CircleB implements ICircle {
  public CircleB(double x, double y, double r) {
      // TODO
    }

    public void moveCenter(double dx, double dy) {
      // TODO
    }

    public void changeRadius(double r) {
      // TODO
    }

    public Point center() {
      return null; // TODO
    }

}
