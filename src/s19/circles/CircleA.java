package s19.circles;

// Variant A, using inheritance
public class CircleA implements ICircle {
  // TODO
  public CircleA(double x, double y, double r) {
    // TODO
  }

  public void moveCenter(double dx, double dy) {
    // TODO
  }

  public void changeRadius(double r) {
    // TODO
  }

  public Point center() {
    return null; // TODO
  }

}
