package s19.circles;

public interface ICircle {
  void moveCenter(double dx, double dy);
  void changeRadius(double r);
  Point center();
}
