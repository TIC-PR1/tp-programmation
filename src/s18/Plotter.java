//==============================================================================
//  HEIA-FR
//==============================================================================
package s18;

// Here our Plotter type is defined as a kind of "motorized pencil"
public class Plotter extends Pencil {
  public Plotter() {   // Initially at (0,0)
    // TODO
    // ...
  }
  
  public void move(int dx, int dy) {
    // TODO
    // ...
  }

  public int xCoord() {
    // TODO
    // ...
    return 0;
  }

  public int yCoord() {
    // TODO
    // ...
    return 0;
  }

  /** Total movements with pencil down, ie length of what has been drawn */
  public double strokeLength() {
    // TODO
    // ...
    return 0;
  }
}
