//==============================================================================
//HEIA-FR / Jacques Bapst
//==============================================================================
package corr.s12;
public class Ex1207_Corr {

  public static void main(String[] args) {
    int[][] res;
    res = allSyracuseFlights(100_000);
    displaySyracuseFlights(res);
  }
  
  //----------------------------------------------------------------------------
  //  Syracuse conjecture
  //----------------------------------------------------------------------------
  public static void displaySyracuseFlights(int[][] res) {
    for (int i=1; i<res.length; i++) {
      System.out.println("Nb : " + i + "   Nb étapes : " + res[i][0]
                       + "   Val max : " + res[i][1]
                       + "   Etapes>n : " + res[i][2]);
    }
  }
  
  public static int[][] allSyracuseFlights(int vMax) {
    int[][] tab = new int[vMax][3];
    for (int i=1; i<vMax; i++) {
      int[] r = syracuse(i);
      tab[i][0] = r[0];
      tab[i][1] = r[1];
      tab[i][2] = r[2];
    }
    return tab;
  }

  // result[0] is the flight length (total number of steps)
  // result[1] is the maximum value reached during the flight (the highest altitude)
  // result[2] is the maximum sub-flight length totally above the starting value (was not asked)
  public static int[] syracuse(int n) {
    int nSteps   = 0; // flight length (total number of steps)
    int highest  = n; // maximum value reached during the flight
    int nInit    = n; // the starting value
    int nbaltmax = 0; // maximum sub-flight length totally above the starting value
    int nbalt    = 0; // current sub-flight length totally above the starting value (or 0 when we're lower)
    while (n != 1) {
      if (n%2 != 0) {
        n = 3*n + 1;
      } else {
        n = n/2;
      }
      nSteps++;
      if (n>highest) highest=n;
      if (n>nInit) nbalt++;
      else         nbalt=0;
      if (nbalt>nbaltmax) nbaltmax=nbalt;
    }
    return new int[] {nSteps, highest, nbaltmax};
  }
}
