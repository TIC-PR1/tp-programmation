package corr.s17.moon;

public class CrashAtLaunchException extends Exception {
  public CrashAtLaunchException() {
    super("Crash at launch");
  }
}
