package corr.s20.ex1_moon;

public class CrashAtLaunchException extends Exception {
  public CrashAtLaunchException() {
    super("Crash at launch");
  }
}
