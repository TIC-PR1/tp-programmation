package s12;

public class Ex1206 {

  public static void main(String[] args) {
    System.out.println(fA (10, 6));
    System.out.println(fAa(10, 6));
    System.out.println(fAb(10, 6));
    System.out.println(fA (10, 0));
    System.out.println(fAa(10, 0));
    System.out.println(fAb(10, 0));
  }
  
  public static int fA(int n, int k) {
    int sum = 0;
    for (int i=k ; i>0 ; i--)
      sum += (n-i);
    return sum;
  }
  
  public static int fAa(int n, int k) {
    // TODO: rewrite using a while() loop instead of a for() loop
    return 0;
  }
  
  public static int fAb(int n, int k) {
    // TODO: rewrite using a do…while() loop instead of a for() loop
    return 0;
  }
}
