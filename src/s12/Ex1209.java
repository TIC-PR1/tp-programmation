//==============================================================================
//HEIA-FR / Jacques Bapst
//==============================================================================
package s12;

import java.util.Arrays;

public class Ex1209 {

  public static void main(String[] args) {
    // It's a paper+pencil exercise; when it's solved, you can verify that the
    // invocations below match your expectations.

    // int[] a = {3,2,1,4,2,3,1,3};
    // int[] b = {3,2,1,3,2,1,3,2};
    // int[] tab = {2,1,3,2,1,3,2,1,3,5};
    // System.out.println(Arrays.toString(myst(a, 3)));
    // System.out.println(Arrays.toString(myst(b, 2)));
    // System.out.println(Arrays.toString(myst(tab, 2)));
  }

  //----------------------------------------------------------------------------
  // TODO: describe the overall effect
  // TODO: determine in which situations the method throws an exception
  // ----------------------------------------------------------------------------
  public static int[] myst(int[] v, int n) {
    int[] r = new int[v.length];
    int m=v[0];
    for (int i=0; i<v.length; i++) {
      if (v[i]>m) m= v[i];
      r[i] = -1;
    }
    int[] t = new int[m+1];
    m=t[0];
    for (int i=0; i<v.length; i++) {
      t[v[i]]++;
      if (t[v[i]]>m) m=t[v[i]];
    }
    int p=0;
    for (int i=m; i>=n; i--) {
     for (int j=0; j<t.length; j++) {
       if (t[j] == i) {
         r[p] = j;
         p++;
       }
     }
   }
   return r;
  }
}