package s12;

public class Ex1205 {

  public static void main(String[] args) {
    // TODO: Analyze the rk() method and describe its overall effect
    // It's a paper+pencil exercise; when it's solved, you can verify that the
    // invocations below match your expectations.

    // System.out.println(rk(26, 3));
    // System.out.println(rk(27, 3));
    // System.out.println(rk(1, 1));
    // System.out.println(rk(123, 2));
  }

  //----------------------------------------------------------------------------
  // Computes ...
  //----------------------------------------------------------------------------
  public static int rk(int n, int k) {
    int i, p=0;
    for (i=1 ; p<=n ; i++) {
      p=i;
      for (int j=1; j<k ; j++) {
        p = p * i;
      }
    }
    return i-2;
  }
}
